# LŮNĬX
This is my NixOS configuration, it uses Home Manager and Flakes.

## Default programs
- **Wayland compositor**: Hyprland
- **Terminal emulator**: foot
- **Bar**: ags
- **Screen locker**: hyprlock
- **Shell**: fish with starship prompt
- **Editor**: neovim
- **GUI file manager**: nemo
- **TUI file manager**: yazi
- **Web browser**: librewolf (flatpak)
- **Image viewer**: nomacs
- **Video player**: mpv
- **Music player**: ncmpcpp
