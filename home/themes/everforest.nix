rec {
  base00 = "2F383E"; # ----
  base01 = "3A454C"; # ---
  base02 = "435058"; # --
  base03 = "4E5C66"; # -
  base04 = "D8D5C2"; # +
  base05 = "E5E2CE"; # ++
  base06 = "F2EED9"; # +++
  base07 = "FDF6E3"; # ++++
  base08 = "E67E80"; # red
  base09 = "E69875"; # orange
  base0A = "DBBC7F"; # yellow
  base0B = "A7C080"; # green
  base0C = "83C092"; # cyan
  base0D = "7FBBB3"; # blue
  base0E = "D699B6"; # purple
  base0F = "EAEDC8"; # brown

  accent = base0B;
  accent2 = base0C;
  wall = "Everforest.jpg";
}
