rec {
  base00 = "1D2021"; # ----
  base01 = "302D2B"; # ---
  base02 = "3C3836"; # --
  base03 = "4C4744"; # -
  base04 = "BDAE93"; # +
  base05 = "D5C4A1"; # ++
  base06 = "EBDBB2"; # +++
  base07 = "FBF1C7"; # ++++
  base08 = "FB4934"; # red
  base09 = "FE8019"; # orange
  base0A = "FABD2F"; # yellow
  base0B = "B8BB26"; # green
  base0C = "8EC07C"; # cyan
  base0D = "83A598"; # blue
  base0E = "D3869B"; # purple
  base0F = "D65D0E"; # brown

  accent = base08;
  accent2 = base0B;
  wall = "Gruvbox.jpg";
}
