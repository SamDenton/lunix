rec {
  base00 = "232136"; # ----
  base01 = "2a273f"; # ---
  base02 = "393552"; # --
  base03 = "6e6a86"; # -
  base04 = "908caa"; # +
  base05 = "e0def4"; # ++
  base06 = "e0def4"; # +++
  base07 = "e0def4"; # ++++
  base08 = "eb6f92"; # red
  base09 = "ea9a97"; # orange
  base0A = "f6c177"; # yellow
  base0B = "9ccfd8"; # green
  base0C = "ea9a97"; # cyan
  base0D = "3e8fb0"; # blue
  base0E = "c4a7e7"; # purple
  base0F = "56526e"; # brown

  accent = base08;
  accent2 = base0B;
  wall = "Rose-Pine-Moon.png";
}
