rec {
  base00 = "2E3440"; # ----
  base01 = "3B4252"; # ---
  base02 = "434C5E"; # --
  base03 = "4C566A"; # -
  base04 = "BCC1CB"; # +
  base05 = "C9CFD9"; # ++
  base06 = "D5DBE5"; # +++
  base07 = "E1E7F2"; # ++++
  base08 = "BF616A"; # red
  base09 = "D08770"; # orange
  base0A = "EBCB8B"; # yellow
  base0B = "A3BE8C"; # green
  base0C = "88C0D0"; # cyan
  base0D = "81A1C1"; # blue
  base0E = "B48EAD"; # purple
  base0F = "5E81AC"; # brown

  accent = base0D;
  accent2 = base0C;
  wall = "Nord.jpg";
}
