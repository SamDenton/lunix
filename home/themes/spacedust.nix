rec {
  base00 = "0A1E24"; # ----
  base01 = "0E2B34"; # ---
  base02 = "123641"; # --
  base03 = "15404D"; # -
  base04 = "C8CCA4"; # +
  base05 = "D4D8AE"; # ++
  base06 = "E1E5B8"; # +++
  base07 = "ECF0C1"; # ++++
  base08 = "FF8A3A"; # red
  base09 = "FFA178"; # orange
  base0A = "FFC878"; # yellow
  base0B = "AECAB8"; # green
  base0C = "83A7B4"; # cyan
  base0D = "67A0CE"; # blue
  base0E = "FF8A3A"; # purple
  base0F = "684C31"; # brown

  accent = base08;
  accent2 = base0B;
  wall = "Spacedust.png";
}
