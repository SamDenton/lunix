rec {
  base00 = "25141F"; # ----
  base01 = "341C2C"; # ---
  base02 = "3F2135"; # --
  base03 = "4C2840"; # -
  base04 = "CBC3B1"; # +
  base05 = "DAD1BE"; # ++
  base06 = "E5DCC8"; # +++
  base07 = "F3E9D4"; # ++++
  base08 = "BE100E"; # red
  base09 = "BE720E"; # orange
  base0A = "EAA549"; # yellow
  base0B = "858162"; # green
  base0C = "989A9C"; # cyan
  base0D = "426A79"; # blue
  base0E = "97522C"; # purple
  base0F = "97672C"; # brown

  accent = base08;
  accent2 = base0E;
  wall = "Belafonte-Night.jpg";
}
