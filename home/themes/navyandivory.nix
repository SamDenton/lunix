rec {
  base00 = "031F26"; # ----
  base01 = "042933"; # ---
  base02 = "053441"; # --
  base03 = "074859"; # -
  base04 = "CBCBA6"; # +
  base05 = "D9D9B2"; # ++
  base06 = "E6E6BC"; # +++
  base07 = "F6F6C9"; # ++++
  base08 = "EF5847"; # red
  base09 = "F06C48"; # orange
  base0A = "BEB090"; # yellow
  base0B = "A2D9B1"; # green
  base0C = "9ED9D8"; # cyan
  base0D = "61778D"; # blue
  base0E = "FF99A1"; # purple
  base0F = "A36631"; # brown

  accent = base08;
  accent2 = base0B;
  wall = "Navy-and-Ivory.png";
}
