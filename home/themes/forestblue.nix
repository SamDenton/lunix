rec {
  base00 = "051519"; # ----
  base01 = "082026"; # ---
  base02 = "0B2B33"; # --
  base03 = "0E3641"; # -
  base04 = "CBC2B8"; # +
  base05 = "D9CFC5"; # ++
  base06 = "E6DCD1"; # +++
  base07 = "F2E7DB"; # ++++
  base08 = "FB3D66"; # red
  base09 = "F8818E"; # orange
  base0A = "30C85A"; # yellow
  base0B = "7DD3A5"; # green
  base0C = "6096BF"; # cyan
  base0D = "39A7A2"; # blue
  base0E = "7E62B3"; # purple
  base0F = "967F4B"; # brown

  accent = base0B;
  accent2 = base0D;
  wall = "Forest-Blue.jpg";
}
