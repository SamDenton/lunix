rec {
  base00 = "0D1D27"; # ----
  base01 = "112633"; # ---
  base02 = "163041"; # --
  base03 = "1A384C"; # -
  base04 = "D9C2B0"; # +
  base05 = "E5CEBA"; # ++
  base06 = "F2D9C4"; # +++
  base07 = "FEE4CE"; # ++++
  base08 = "D48678"; # red
  base09 = "FCA02F"; # orange
  base0A = "FDD39F"; # yellow
  base0B = "628D98"; # green
  base0C = "87ACB4"; # cyan
  base0D = "1BBCDD"; # blue
  base0E = "BBE3EE"; # purple
  base0F = "DEB88D"; # brown

  accent = base0D;
  accent2 = base0A;
  wall = "Seashells.jpg";
}
