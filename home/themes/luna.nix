rec {
  base00 = "0f121a"; # ----
  base01 = "171b27"; # ---
  base02 = "212637"; # --
  base03 = "313952"; # -
  base04 = "A3A6B3"; # +
  base05 = "B9BCCB"; # ++
  base06 = "D0D3E5"; # +++
  base07 = "E2E5F8"; # ++++
  base08 = "899AFF"; # red
  base09 = "8B89FF"; # orange
  base0A = "B0EAD9"; # yellow
  base0B = "48BCA7"; # green
  base0C = "95A7CC"; # cyan
  base0D = "477AB3"; # blue
  base0E = "7882BF"; # purple
  base0F = "6673BF"; # brown

  accent = base08;
  accent2 = base0B;
  wall = "Luna.png";
}
