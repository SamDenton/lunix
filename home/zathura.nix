{ theme }: {
  programs.zathura = with theme; {
    enable = true;
    extraConfig = ''
      set recolor true
      set guioptions none
      set notification-error-bg "#${base01}"
      set notification-error-fg "#${base09}"
      set notification-warning-bg "#${base01}"
      set notification-warning-fg "#${base09}"
      set notification-bg "#${base01}"
      set notification-fg "#${base09}"
      set completion-group-bg "#${base01}"
      set completion-group-fg "#${accent2}"
      set completion-bg "#${base00}"
      set completion-fg "#${base07}"
      set completion-highlight-bg "#${accent2}"
      set completion-highlight-fg "#${base01}"
      set inputbar-bg "#${base00}"
      set inputbar-fg "#${base07}"
      set statusbar-bg "#${base01}"
      set statusbar-fg "#${base07}"
      #set highlight-color "#${accent2}"
      #set highlight-active-color "#${accent}"
      set default-bg "#${base01}"
      set default-fg "#${base07}"
      set recolor-lightcolor "#${base00}"
      set recolor-darkcolor "#${base07}"
      set index-bg "#${base00}"
      set index-fg "#${base07}"
      set index-active-bg "#${accent2}"
      set index-active-fg "#${base01}"
    '';
  };
}
