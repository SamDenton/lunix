{ inputs, config, pkgs, ... }:
let
  theme = import ./themes/luna.nix;
in
{
  imports = [
    (import ./hyprland.nix { inherit theme; })
    (import ./neovim.nix { inherit theme pkgs; })
    (import ./gtkqt.nix { inherit theme; })
    (import ./terminal.nix { inherit theme; })
    (import ./hypr-tools.nix { inherit theme; })
    (import ./music.nix { inherit theme; })
    (import ./zathura.nix { inherit theme; })
    inputs.ags.homeManagerModules.default
  ];

  home = {
    username = "sam";
    homeDirectory = "/home/sam";
    file.".local/bin/scripts" = {
      source = ./scripts;
      executable = true;
    };
  };

  programs.ags = {
    enable = true;
        #configDir = ./ags;
        # extraPackages = with pkgs; [
        #   inputs.ags.packages.${pkgs.system}.battery
        #   inputs.ags.packages.${pkgs.system}.apps
        #   inputs.ags.packages.${pkgs.system}.hyprland
        #   inputs.ags.packages.${pkgs.system}.bluetooth
        #   inputs.ags.packages.${pkgs.system}.notifd
        #   inputs.ags.packages.${pkgs.system}.tray
        #   inputs.ags.packages.${pkgs.system}.mpris
        #   inputs.ags.packages.${pkgs.system}.wireplumber
        #   inputs.ags.packages.${pkgs.system}.network
        # ];
  };

  # Miscellaneous program configs
  xdg.mimeApps.defaultApplications = {
    "text/plain" = [ "neovim.desktop" ];
    "application/pdf" = [ "zathura.desktop" ];
    "image/*" = [ "nomacs.desktop" ];
    "video/*" = [ "mpv.desktop" ];
  };

  programs.git = {
    enable = true;
    userName = "Sam Denton";
    userEmail = "12024713-SamDenton@users.noreply.gitlab.com";
  };

  programs.yt-dlp = {
    enable = true;
    extraConfig = ''
      -o "~/Downloads/YouTube/%(title)s.%(ext)s"
      #-o "~/Downloads/YouTube/%(uploader)s/%(title)s.%(ext)s"
    '';
  };

  programs.mpv = {
    enable = true;
    config = {
      fs = false;
      ontop = true;
      sub-auto = false;
      autofit= "11%";
    };
  };

  programs.home-manager.enable = true;

  # https://nixos.wiki/wiki/FAQ/When_do_I_update_stateVersion
  home.stateVersion = "24.11";
}
