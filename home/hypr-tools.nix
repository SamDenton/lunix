{ theme }: {

  home.file.".config/hypr/hypridle.conf".text = ''
    general {
        lock_cmd = pidof hyprlock || hyprlock       # avoid starting multiple hyprlock instances.
        before_sleep_cmd = loginctl lock-session    # lock before suspend.
        after_sleep_cmd = hyprctl dispatch dpms on  # to avoid having to press a key twice to turn on the display.
    }

    listener {
        timeout = 480                                 # 8min
        on-timeout = hyprctl dispatch dpms off        # screen off when timeout has passed
        on-resume = hyprctl dispatch dpms on          # screen on when activity is detected after timeout has fired.
    }

    listener {
        timeout = 510                                 # 8.5min
        on-timeout = loginctl lock-session            # lock screen when timeout has passed
    }
  '';

  home.file.".config/hypr/hyprlock.conf".text = with theme; ''
  general {
      hide_cursor = true
  }
  background {
      # color = rgb(${base00})
      path = /home/sam/Pictures/Wallpapers/Lunix/${wall}
      blur_passes = 2 # 0 disables blurring
      blur_size = 7
      noise = 0.0117
      contrast = 0.8916
      brightness = 0.8172
      vibrancy = 0.1696
      vibrancy_darkness = 0.0
  }
  image {
      path = /home/sam/Pictures/Wallpapers/Lunix/sam.png
      size = 448 # lesser side if not 1:1 ratio
      rounding = -1 # negative values mean circle
      border_size = 5
      border_color = rgb(${base01})
      rotate = 0 # degrees, counter-clockwise
      reload_time = -1 # seconds between reloading, 0 to reload with SIGUSR2
      reload_cmd =  # command to get new path. if empty, old path will be used. don't run "follow" commands like tail -F

      position = 0, 420
      halign = center
      valign = center
  }
  input-field {
      size = 450, 75
      outline_thickness = 5
      dots_size = 0.33 # Scale of input-field height, 0.2 - 0.8
      dots_spacing = 0.15 # Scale of dots' absolute size, 0.0 - 1.0
      dots_center = false
      dots_rounding = -1 # -1 default circle, -2 follow input-field rounding
      outer_color = rgb(${base02})
      inner_color = rgb(${base06})
      font_color = rgb(${base01})
      fade_on_empty = true
      fade_timeout = 1000 # Milliseconds before fade_on_empty is triggered.
      placeholder_text = <i>Input Password...</i> # Text rendered in the input box when it's empty.
      hide_input = false
      rounding = -1 # -1 means complete rounding (circle/oval)
      check_color = rgb(${base0A})
      fail_color = rgb(${base08}) # if authentication failed, changes outer_color and fail message color
      fail_text = <i>$FAIL <b>($ATTEMPTS)</b></i> # can be set to empty
      fail_transition = 300 # transition time in ms between normal outer_color and fail_color
      capslock_color = -1
      numlock_color = -1
      bothlock_color = -1 # when both locks are active. -1 means don't change outer color (same for above)
      invert_numlock = false # change color if numlock is off
      swap_font_color = false # see below

      position = 0, -20
      halign = center
      valign = center
  }
  label {
      text = SÅM
      color = rgb(${base07})
      font_size = 50
      font_family = Noto Sans
      rotate = 0 # degrees, counter-clockwise

      position = 0, 120
      halign = center
      valign = center
  }
  '';
}
