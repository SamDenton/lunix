{ pkgs, theme }: {
  programs.neovim = with theme; {
    enable = true;
    defaultEditor = true;

    viAlias = true;
    vimAlias = true;
    vimdiffAlias = true;

    extraLuaConfig = ''
      local g = vim.g
      local o = vim.o

      local function map(m, k, v)
        vim.keymap.set(m, k, v, { silent = true })
      end

      o.expandtab = true
      o.tabstop = 4
      o.shiftwidth = 4
      o.relativenumber = true
      o.hlsearch = false
      o.smartindent = true
      o.nu = true
      o.ignorecase = true
      o.smartcase = true
      o.title = true
      o.backup = false
      o.termguicolors = false
      o.wildmode = "longest,list,full"
      o.mouse = "a"

      g.loaded_netrw = 1
      g.loaded_netrwPlugin = 1
      g.mapleader = " "

      map("n", "<leader>w", "<CMD>w<CR>")
      map("n", "<leader>q", "<CMD>q<CR>")
      map("n", "<leader>x", "<CMD>x<CR>")
      map("n", "<leader>s", "<CMD>setlocal spell!<CR>")
      map("n", "<leader>e", "<CMD>NvimTreeToggle<CR>")
      map("n", "<leader>z", "<CMD>ZenMode<CR>")
      vim.cmd "colorscheme vim"
      vim.cmd[[autocmd BufWritePre * %s/\s\+$//e]]
      vim.cmd[[autocmd BufWritePre * %s/\n\+\%$//e]]
    '';

    plugins = with pkgs.vimPlugins; [
      {
        plugin = nvim-tree-lua;
        type = "lua";
        config = "require('nvim-tree').setup {}";
      }
      {
        plugin = nvim-web-devicons;
        type = "lua";
        config = "require('nvim-web-devicons').setup {}";
      }
      {
        plugin = bufferline-nvim;
        type = "lua";
        config = ''
                  require('bufferline').setup {
                      options = {
                          offsets = {
                          {
                              filetype = "NvimTree",
                              text = "File Explorer",
                              separator = true
                          }
                      },
                      }
          	}
        '';
      }
      {
        plugin = zen-mode-nvim;
        type = "lua";
        config = ''
          require('zen-mode').setup {
            window = {
               width = 0.6,
               height = 0.75,
              options = {
                number = false,
                relativenumber = false,
              },
            },
          }
        '';
      }
      {
        plugin = lualine-nvim;
        type = "lua";
        config = ''
                require('lualine').setup {
                  options = {
                    theme = '16color',
                    component_separators = { left = "", right = "❘"},
                    section_separators = { left = "", right = ""},
                    },
                    sections = {
                            lualine_a = {
            { 'mode', separator = { left = '' }, right_padding = 2  },
          },
                      lualine_b = {'branch','diagnostics'},
                      lualine_c = {'filename'},
                      lualine_x = {'encoding', 'fileformat', 'filetype'},
                      lualine_y = {'progress'},
                          lualine_z = {
            { 'location', separator = { right = '' }, left_padding = 2 },
          },
                    }
                  }
        '';
      }
      {
        plugin = (nvim-treesitter.withPlugins (p: [
          p.tree-sitter-nix
          p.tree-sitter-vim
          p.tree-sitter-bash
          p.tree-sitter-lua
          p.tree-sitter-python
          p.tree-sitter-json
          p.tree-sitter-c
          p.tree-sitter-html
          p.tree-sitter-css
          p.tree-sitter-javascript
        ]));
        type = "lua";
        config = ''
          require('nvim-treesitter.configs').setup {
            ensure_installed = {},
            auto_install = false,
            highlight = { enable = true },
            indent = { enable = true },}
        '';
      }
    ];
  };
}
