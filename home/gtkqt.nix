{ theme }:
let
  gtkCss = with theme; ''
    @define-color accent_color #${accent};
    @define-color accent_color2 #${accent2};
    @define-color red #${base08};
    @define-color accent_bg_color mix(#${base0D}, #${base00},0.3);
    @define-color accent_fg_color #${base05};
    @define-color destructive_color #${base0D};
    @define-color destructive_bg_color mix(#${base0D}, #${base00},0.3);
    @define-color destructive_fg_color #${base05};
    @define-color success_color mix(#${base0B},white,0.2);
    @define-color success_bg_color mix(#${base0B},black,0.2);
    @define-color success_fg_color #${base05};
    @define-color warning_color mix(#${base0A},white,0.2);
    @define-color warning_bg_color mix(#${base0A},black,0.2);
    @define-color warning_fg_color rgba(0, 0, 0, 0.8);
    @define-color error_color mix(#${base08},white,0.2);
    @define-color error_bg_color mix(#${base0D}, #${base00},0.3);
    @define-color error_fg_color #${base05};
    @define-color window_bg_color #${base00};
    @define-color window_fg_color #${base05};
    @define-color view_bg_color #${base00};
    @define-color view_fg_color #${base05};
    @define-color headerbar_bg_color mix(#${base00},black,0.2);
    @define-color headerbar_fg_color #${base05};
    @define-color headerbar_border_color #${base05};
    @define-color headerbar_backdrop_color @window_bg_color;
    @define-color headerbar_shade_color rgba(0, 0, 0, 0.36);
    @define-color card_bg_color rgba(255, 255, 255, 0.08);
    @define-color card_fg_color #${base05};
    @define-color card_shade_color rgba(0, 0, 0, 0.36);
    @define-color dialog_bg_color #${base01};
    @define-color dialog_fg_color #${base05};
    @define-color popover_bg_color #${base01};
    @define-color popover_fg_color #${base05};
    @define-color shade_color rgba(0,0,0,0.36);
    @define-color scrollbar_outline_color rgba(0,0,0,0.5);
  '';
in
{
  qt = {
    enable = true;
    platformTheme.name = "gtk";
    style.name = "adwaita-dark";
  };
  gtk = {
    enable = true;
    font.name = "Cantarell";
    font.size = 12;
    theme.name = "adw-gtk3";
    iconTheme.name = "Papirus";
    cursorTheme.name = "Bibata-Modern-Ice";
    gtk3.extraCss = gtkCss;
    gtk4.extraCss = gtkCss;
    gtk2.configLocation = "/home/sam/.config/gtk-2.0/gtkrc";
  };
}
