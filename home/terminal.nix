{ theme }: {

  home.shellAliases = {
    u = "sudo nixos-rebuild switch --flake $HOME/.config/lunix && lunix -h && flatpak update -y";
    nr = "sudo nixos-rebuild boot --flake $HOME/.config/lunix";
    nrs = "sudo nixos-rebuild switch --flake $HOME/.config/lunix";
    nu = "nix flake update --flake $HOME/.config/lunix/";
    hm = "lunix -h";


    ip = "ip -color=auto";
    ls = "eza --icons --group-directories-first";
    la = "eza -la --icons --group-directories-first";
    ll = "eza -l --icons --group-directories-first";

    s = "sudo";
    v = "nvim";
    e = "yazi";
    p = "pulsemixer";
    n = "ncmpcpp";
    z = "__zoxide_z";
    c = "__zoxide_z";
    ux = "chmod u+x";
    wttr = "curl wttr.in";
    fonts = "fc-list --format='%{family[0]}\n' | sort | uniq";

    rm = "rm -i";
    cp = "cp -ip";
    mv = "mv -i";
    ln = "ln -i";

    g = "git";
    ga = "git add";
    gr = "git restore";
    gs = "git status";
    gd = "git diff";
    gl = "git log";
    gc = "git commit -m";
    gca = "git commit -am";
    gcl = "git clone --depth 1";
    gp = "git push";
    gpl = "git pull";

    yt = "yt-dlp";
    yt-720 = "yt -f 136+140";
    yt-1080 = "yt -f 137+140";
    yt-m4a = "yt -f 140";
  };

  programs.foot = with theme; {
     enable = true;
     settings = {
       main = {
         pad = "60x40";
         shell = "fish";
         font = "AnonymiceProNerdFont:size=15";
       };
       scrollback.indicator-position= "none";
       colors = {
         alpha = "0.5";
         background = "${base00}";
         foreground = "${base07}";
         regular0 = "${base02}";
         regular1 = "${base08}";
         regular2 = "${base0B}";
         regular3 = "${base0A}";
         regular4 = "${base0D}";
         regular5 = "${base0E}";
         regular6 = "${base0C}";
         regular7 = "${base07}";
         bright0 = "${base03}";
         bright1 = "${base08}";
         bright2 = "${base0B}";
         bright3 = "${base0A}";
         bright4 = "${base0D}";
         bright5 = "${base0E}";
         bright6 = "${base0C}";
         bright7 = "${base07}";
       };
     };
   };

  programs.tmux = {
    enable = true;
    extraConfig = ''
      # Change Tmux key, set the windows to be indexed by 1 and add mouse support
      set -g prefix C-space
      set -g base-index 1
      set -g mouse

      set -g default-shell ~/.local/state/nix/profile/bin/fish

      # Status bar style
      set-option -g status-style "bg=black,fg=white"
      set-option -g status-right ""
      set -g status

      # Window title
      set -g set-titles on
      set -g set-titles-string '#S - #I/#{session_windows} #W '

      # Create 5 windows on startup
      new -s "tmux" -n " dev"
      neww -n "󱡶 sys"
      neww -n " lf"
      neww -n "󰱠 ssh"
      neww -n "󰒟 etc"
      next

      # Status bar toggle
      bind -r t set -g status

      # Vim like pane resizing
      bind -r C-h resize-pane -L
      bind -r C-j resize-pane -D
      bind -r C-k resize-pane -U
      bind -r C-l resize-pane -R

      # Vim like pane/window switching
      bind -r ^ last-window
      bind -r h select-pane -L
      bind -r j select-pane -D
      bind -r k select-pane -U
      bind -r l select-pane -R

      # Select windows with homerow keys
      bind -r a select-window -t 1 \; set -g status \; set -g status
      bind -r s select-window -t 2
      bind -r d select-window -t 3
      bind -r f select-window -t 4
      bind -r g select-window -t 5

      # Rebind pane commands
      bind -r q kill-pane
      bind -r v split-window -v -c "#{pane_current_path}"
      bind -r b split-window -h -c "#{pane_current_path}"
    '';
  };

  programs.fish = {
    enable = true;
    interactiveShellInit = ''
      set fish_greeting # Disable greeting
      set fish_color_normal cyan
      set fish_color_autosuggestion brblack
      set fish_color_command green
      set fish_color_error red
      set fish_color_param brwhite

      zoxide init fish | source
      starship init fish | source
    '';
  };

  programs.starship = {
    enable = true;
    settings = {
      add_newline = false;
      line_break.disabled = true;
      character.success_symbol = "[❯](green)";
      package.disabled = true;
    };
    enableFishIntegration = false;
  };
}
