{ theme }: {
  programs.ncmpcpp = {
    enable = true;
    mpdMusicDir = "~/Music";
    bindings = [
      { key = "j"; command = "scroll_down"; }
      { key = "k"; command = "scroll_up"; }
      { key = "h"; command = "jump_to_parent_directory"; }
      { key = "l"; command = "enter_directory"; }
      { key = "l"; command = "run_action"; }
      { key = "l"; command = "play_item"; }
      { key = "h"; command = "previous_column"; }
      { key = "l"; command = "next_column"; }
    ];
    settings = {
      ncmpcpp_directory = "~/.config/ncmpcpp";
      lyrics_directory = "~/.local/share/lyrics";

      message_delay_time = "1";
      autocenter_mode = "yes";
      centered_cursor = "yes";
      ignore_leading_the = "yes";
      allow_for_physical_item_deletion = "no";

      progressbar_look = "▂▂▂";
      progressbar_color = "black";
      progressbar_elapsed_color = "cyan";

      alternative_header_first_line_format = "$b$5󰎈$/b  $b{%t}|{%f}$/b $/b";
      alternative_header_second_line_format = "{$b{$2  %a$9}{ - $3󰀥  %b$9}{ $4[󰭦  %y]}}|{%D}";

      startup_screen = "browser";
      display_volume_level = "yes";
      external_editor = "nvim";
      use_console_editor = "yes";

      colors_enabled = "yes";
      main_window_color = "white";
      header_window_color = "cyan";
      volume_color = "cyan";
      statusbar_color = "cyan";
      empty_tag_color = "magenta";
      statusbar_time_color = "cyan:b";

      user_interface = "alternative";
    };
  };
  services.mpd = {
    enable = true;
    musicDirectory = "/home/sam/Music";
    extraConfig = ''
      playlist_directory      "/home/sam/Music/1.Playlists"
      db_file                 "/home/sam/.cache/mpd.db"
      bind_to_address         "127.0.0.1"
      port                    "6600"

      auto_update             "yes"

      audio_output {
          type            "pipewire"
          name            "Pipewire Sound Server"
      }

      audio_output {
         type	"fifo"
         name	"my_fifo"
         path	"/tmp/mpd.fifo"
         format	"44100:16:2"
      }
    '';
    network.startWhenNeeded = true;
  };
  programs.cava = with theme; {
    #enable = true;
    settings = {
      color = {
        gradient = 1;
        gradient_count = 2;
        gradient_color_1 = "'#${accent}'";
        gradient_color_2 = "'#${accent2}'";
      };
    };
  };
}
