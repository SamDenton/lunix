import { Clock } from "./buttons/clock.js"
import { SysTray } from "./buttons/systray.js"
import { ClientTitle } from "./buttons/clienttitle.js"
import { Volume } from "./buttons/volume.js"
import { Workspaces } from "./buttons/workspaces.js"

// layout of the bar
function Left() {
    return Widget.Box({
        class_name: 'bar_left',
        hpack: 'start',
        spacing: 8,
        children: [
            Workspaces(),
        ],
    })
}


function Center() {
    return Widget.Box({
        class_name: 'bar_center',
        spacing: 8,
        children: [
            ClientTitle(),
        ],
    })
}


function Right() {
    return Widget.Box({
        class_name: 'bar_right',
        hpack: "end",
        spacing: 8,
        children: [
            SysTray(),
            Volume(),
            Clock(),
        ],
    })
}


export function Bar(monitor = 0) {
    return Widget.Window({
        name: `bar-${monitor}`, // name has to be unique
        class_name: "bar",
        monitor,
        anchor: ["top", "left", "right"],
        margins: [10 , 10 , 0 ,10 ],
        exclusivity: "exclusive",
        child: Widget.CenterBox({
            start_widget: Left(),
            center_widget: Center(),
            end_widget: Right(),
        }),
    })
}
