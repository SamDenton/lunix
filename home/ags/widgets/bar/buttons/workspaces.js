const hyprland = await Service.import("hyprland")

const labels = [ "󰖟", "󰆍","󰉋", "󰖷", "󰊗" ];
const dispatch = ws => hyprland.messageAsync(`dispatch workspace ${ws}`);

export const Workspaces = ws => Widget.Box({
    class_name: "workspaces",
    children: Array.from({ length: 5 }, (_, i) => i + 1).map(i => Widget.Button({
        attribute: i,
        label: labels[i - 1],
        onClicked: () => dispatch(i),
        setup: self => self.hook(hyprland, () => {
            self.toggleClassName("active", hyprland.active.workspace.id === i)
            self.toggleClassName("occupied",hyprland.active.workspace.id !== i &&  (hyprland.getWorkspace(i)?.windows || 0) > 0)
        }),
    })),
})
