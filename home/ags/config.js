import { Bar } from "./widgets/bar/bar.js"
import { applauncher } from "./widgets/applauncher.js"
import { NotificationPopups } from "./widgets/notificationPopups.js"

App.config({
    style: "./style/style.css",
    windows: [
        NotificationPopups(),
        Bar(),
        applauncher,
    ],
})
