{ theme }: {
  wayland.windowManager.hyprland = with theme; {
    enable = true;
    systemd.enable = false;
    settings = {
      xwayland = {
        force_zero_scaling = true;
        use_nearest_neighbor = false;
      };
      monitor = [
        "DP-1,3840x2160@144,auto,2"
        "eDP-1,1920x1080,auto,1.25"
      ];
      exec-once = [
        "hypridle"
        "gsettings set $gnome-schema cursor-theme 'Bibata-Modern-Ice'"
        "hyprctl setcursor Bibata-Modern-Ice 24"
        "ags"
      ];
      exec = [
        "swaybg -i /home/sam/Pictures/Wallpapers/Lunix/${wall} -m fill"
      ];
      env = [
        "XCURSOR_THEME,Bibata-Modern-Ice"
        "XCURSOR_SIZE,24"
      ];
      input = {
        repeat_rate = 40;
        sensitivity = 0;
        touchdevice = {
          enabled = false;
        };
      };
      cursor = {
        inactive_timeout = 10;
      };
      general = {
        gaps_in = 5;
        gaps_out = 10;
        border_size = 2;
        "col.active_border" = "rgb(${accent}) rgb(${accent2}) 30deg";
        "col.inactive_border" = "rgb(${base01})";
        layout = "master";
      };
      decoration = {
        rounding = 10;
        shadow = {
          color = "rgba(${base00}ee)";
        };
        blur = {
          noise = 0.02;
          passes = 3;
        };
      };
      animations = {
        enabled = true;
        animation = [
          "windows, 1, 6, default"
          "windowsOut, 1, 7, default, popin 80%"
          "border, 1, 10, default"
          "borderangle, 1, 8, default"
          "fade, 1, 7, default"
          "workspaces, 1, 5, default"
          "specialWorkspace, 1, 2, default, fade"
        ];
      };
      gestures = {
        workspace_swipe = true;
      };
      misc = {
        disable_hyprland_logo = true;
      };
      ecosystem = {
        no_update_news = true;
        no_donation_nag = true;
      };
      workspace = [
        "w[tv1], gapsout:0, gapsin:0"
        "f[1], gapsout:0, gapsin:0"
      ];

      windowrule = [
        "workspace 1,^(LibreWolf)$"
        "workspace 2,^(foot)$"
        "workspace 3,^(nemo)$"
        "workspace 4,^(.virt-manager-wrapped)$"
        "workspace 4,^(krita)$"
        "workspace 4,^(obs)$"
        "workspace 4,^(Gimp-2.10)$"
        "workspace 4,^(krita)$"
        "workspace 4,^(guitarix)$"
        "workspace 4,^(blender)$"
        "workspace 4,^(Godot)$"
        "workspace 5,^(steam)$"
        "workspace 5,^(dolphin-emu)$"
        "workspace 5,^(org.libretro.RetroArch)$"
        "workspace 5,^(org.remmina.Remmina)$"
        "float,^(org.keepassxc.KeePassXC)$"
        "float,^(org.nomacs.ImageLounge)$"
        "size 83%,^(org.nomacs.ImageLounge)$"
        "float,^(org.pwmt.zathura)$"
        "size 55% 90%,^(org.pwmt.zathura)$"
        "float,^(org.gnome.FileRoller)$"
        "float,^(xdg-desktop-portal-gtk)$"
        "float,^(.bulky-wrapped)$"
        "float,^(KeePassXc)$"
        "float,^(mousepad)$"
        "float,^(net-runelite-client-RuneLite)$"
        "pin,^(net-runelite-client-RuneLite)$"
        "float,^(mpv)$"
        "pin,^(mpv)$"
      ];
      windowrulev2 = [
        "workspace special, class:^(foot)$,title:^(Scratchpad)$"
        "float, class:^(foot)$,title:^(Scratchpad)$"
        "size 83%, class:^(foot)$,title:^(Scratchpad)$"
        "centerwindow, class:^(foot)$,title:^(Scratchpad)$"
        "stayfocused, class:^(foot)$,title:^(Scratchpad)$"
        "float, class:^(LibreWolf)$,title:^(Library)$"
        "float, class:^(LibreWolf)$,title:^(Picture-in-Picture)$"
        "pin, class:^(LibreWolf)$, title:^(Picture-in-Picture)$"
        "size 25%, class:^(LibreWolf)$, title:^(Picture-in-Picture)$"
        "move 73%, class:^(LibreWolf)$, title:^(Picture-in-Picture)$"
        "float, class:^(LibreWolf)$,title:^(Firefox)$"
        "pin, class:^(LibreWolf)$, title:^(Firefox)$"
        "size 25%, class:^(LibreWolf)$, title:^(Firefox)$"
        "move 73%, class:^(LibreWolf)$, title:^(Firefox)$"
        "center, class:^(steam)$, title:^(Sign in to Steam)"
        "center, class:^(steam)$, title:^(Shutdown)"
        "center, class:^(steam)$, title:^(Special Offers)"
        "center, , initialtitle:^(Godot)$"
        "float, class:^(nemo)$,title:(.*)(Properties)"

        "bordersize 0, floating:0, onworkspace:w[tv1]"
        "rounding 0, floating:0, onworkspace:w[tv1]"
        "bordersize 0, floating:0, onworkspace:f[1]"
        "rounding 0, floating:0, onworkspace:f[1]"
      ];
      bind = [
        "CONTROL ALT, backspace, exec, killall .Hyprland-wrapp"
        "SUPER, return, exec, foot -e tmux attach"
        "SUPER SHIFT, return, exec, foot"
        "SUPER, w, exec, flatpak run io.gitlab.librewolf-community"
        "SUPER, e, exec, nemo"
        "SUPER, r, exec, ags -t applauncher"
        "SUPER, y, exec, virt-manager"
        "SUPER, p, exec, flatpak run com.valvesoftware.Steam"
        "SUPER CONTROL, q, exec, rofi-kill"
        "SUPER, b, exec, pgrep .ags-wrapped > /dev/null && killall .ags-wrapped || ags &"
        "SUPER, t, exec, hypr-scratchpad"
        "SUPER CONTROL, c, exec, hypr-gamemode"
        "SUPER, semicolon, exec, hyprlock"
        "SUPER, c, exec, rofi-lunix"
        "SUPER CONTROL, b, exec, rofi-bluetooth"
        "SUPER, space, togglefloating,"
        "SUPER, q, killactive,"
        "SUPER CONTROL ALT, q, exit,"
        "SUPER, z, fullscreen"
        "SUPER, v, exec, hypr-fzs"
        "SUPER, x, pin"
        "SUPER, a, workspace, 1"
        "SUPER, s, workspace, 2"
        "SUPER, d, workspace, 3"
        "SUPER, f, workspace, 4"
        "SUPER, g, workspace, 5"
        "SUPER, escape, togglespecialworkspace, scratchpad2"
        "SUPER SHIFT, A, movetoworkspace, 1"
        "SUPER SHIFT, S, movetoworkspace, 2"
        "SUPER SHIFT, D, movetoworkspace, 3"
        "SUPER SHIFT, F, movetoworkspace, 4"
        "SUPER SHIFT, G, movetoworkspace, 5"
        "SUPER SHIFT, escape, movetoworkspace, special:scratchpad2"
        "SUPER, mouse_down, workspace, e+1"
        "SUPER, mouse_up, workspace, e-1"
        "SUPER, h, focusmonitor, -1"
        "SUPER, j, cyclenext, "
        "SUPER, k, cyclenext, prev"
        "SUPER, l, focusmonitor, +1"
        ", Print, exec, sshot"
        "SHIFT, Print, exec, sshot -g"
        ", XF86AudioMute, exec, fnkey -vt"
        "SHIFT, XF86_AudioPlay, exec, fnkey -ct"
        ", XF86AudioPrev, exec, mpc prev"
        ", XF86AudioPlay, exec, mpc toggle"
        ", XF86AudioNext, exec, mpc next"
      ];
      binde = [
        ", XF86AudioLowerVolume, exec, fnkey -vd"
        ", XF86AudioRaiseVolume, exec, fnkey -vi"
        "SHIFT, XF86AudioPrev, exec, mpc seek -1"
        "SHIFT, XF86AudioNext, exec, mpc seek +2"
        ", XF86MonBrightnessDown, exec, fnkey -bd"
        ", XF86MonBrightnessUp, exec, fnkey -bi"
        "SHIFT, XF86_AudioLowerVolume, exec, fnkey -md"
        "SHIFT, XF86_AudioRaiseVolume, exec, fnkey -mi"
      ];
      bindm = [
        "SUPER, mouse:272, movewindow"
        "SUPER, mouse:273, resizewindow 1"
        "SUPER SHIFT, mouse:273, resizewindow 2"
      ];
    };
  };
}
