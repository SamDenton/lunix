#!/usr/bin/env sh
# Post install script

# Make directories
mkdir -p $HOME/.local/share/icons
mkdir $HOME/.local/share/themes

# Add flathub, install applications and add overrides
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
flatpak install flathub io.gitlab.librewolf-community -y
flatpak install flathub com.github.tchx84.Flatseal -y
flatpak override --user --filesystem=$HOME/.local/share/icons
flatpak override --user --filesystem=$HOME/.local/share/themes
flatpak override --user --filesystem=$HOME/Downloads

# Install Papirus icon theme. The folder colors can't be changed when it's installed with nix.
wget -qO- https://git.io/papirus-icon-theme-install | DESTDIR="$HOME/.local/share/icons" sh

# Install Bibata mouse icon theme. The cursor theme doesn't work in flatpaks applications when it's installed with nix.
wget https://github.com/ful1e5/Bibata_Cursor/releases/download/v2.0.4/Bibata-Modern-Ice.tar.xz
tar -xvf Bibata-Modern-Ice.tar.xz
rm -f Bibata-Modern-Ice.tar.xz
mv Bibata-Modern-Ice ~/.local/share/icons/

# Install adw-gtk theme. The latest version screws up the gtk.css colors.
wget https://github.com/lassekongo83/adw-gtk3/releases/download/v4.8/adw-gtk3v4-8.tar.xz
tar -xvf adw-gtk3v4-8.tar.xz
rm -f adw-gtk3v4-8.tar.xz
mv adw-gtk3* $HOME/.local/share/themes
