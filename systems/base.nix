{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    # TUI
    yazi
    fzf
    htop
    pulsemixer
    ttyper

    # CLI
    wget
    eza
    zoxide
    pciutils
    mpc-cli
    home-manager
    bat
    killall
    file
    p7zip
    unzip
    lm_sensors
    ripgrep
    fd
    tealdeer
    git
    imagemagick

    # Wayland utilities
    swaybg
    hyprlock
    hypridle
    wl-clipboard
    waycheck
    wev
    grim
    slurp

    # System tools/multimedia
    nemo-with-extensions
    nomacs
    bulky
    papirus-folders
    czkawka
    keepassxc
    ffmpeg
    file-roller
    gnome-disk-utility
    xfce.mousepad
    celluloid

    # Fun
    ponysay # /)
    cowsay
    cmatrix
    asciiquarium
    cbonsai

    # Other
    ffmpegthumbnailer
    xdg-utils
    libnotify
    glib
    dconf
    sddm-chili-theme
    python3
    fastfetch
  ];

  fonts.packages = with pkgs; [
    dejavu_fonts
    google-fonts
    noto-fonts
    noto-fonts-cjk-sans
    noto-fonts-emoji
    liberation_ttf
    anonymousPro
    roboto
    roboto-mono
    roboto-serif
    freefont_ttf
    fira-code
    hanazono
    nerd-fonts.anonymice
    nerd-fonts.symbols-only
  ];

  # User
  users.users.sam = {
    isNormalUser = true;
    description = "sam";
    extraGroups = [ "networkmanager" "wheel" "libvirtd" ];
  };

  # Hyprland
  programs.hyprland = {
    enable = true;
    withUWSM = true;
  };

  # Services/system settings
  services.flatpak.enable = true;
  services.gvfs.enable = true;
  networking.networkmanager.enable = true;

  services.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
  };

  #hardware.bluetooth.enable = true;
  #hardware.bluetooth.powerOnBoot = true;
  #services.blueman.enable = true;

  # Environment variables. The XDG stuff prevents nix from putting symlinks into the home folder.
  nix.settings.use-xdg-base-directories = true;
  environment.sessionVariables = rec {
    XDG_CACHE_HOME = "$HOME/.cache";
    XDG_CONFIG_HOME = "$HOME/.config";
    XDG_DATA_HOME = "$HOME/.local/share";
    XDG_STATE_HOME = "$HOME/.local/state";
    XDG_BIN_HOME = "$HOME/.local/bin";
    HISTFILE = "${XDG_STATE_HOME}/bash/history";
    PATH = [
      "${XDG_BIN_HOME}"
      "${XDG_BIN_HOME}/scripts"
    ];
  };

  # NixOS/Nix settings
  system.stateVersion = "24.11";
  nixpkgs.config.allowUnfree = true;
  nix.settings.experimental-features = [ "nix-command" "flakes" ];
  boot.kernelPackages = pkgs.linuxPackages_latest;
  programs.command-not-found.enable = false;

  time.timeZone = "America/New_York";
  i18n.defaultLocale = "en_US.UTF-8";
}
