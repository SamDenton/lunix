{ config, pkgs, ... }:

{
  imports =
    [
      ./daedalus-hardware.nix
      ./base.nix
    ];

  # Hostname
  networking.hostName = "Daedalus";

  # Packages
  environment.systemPackages = with pkgs; [
    krita
    brightnessctl
    nvtopPackages.amd
  ];

  # Hardware
  hardware.opentabletdriver.enable = true;

  services.pipewire = {
    enable = true;
    wireplumber = {
      enable = true;
      extraConfig = {
        "10-disable-camera" = {
          "wireplumber.profiles" = {
            main."monitor.libcamera" = "disabled";
          };
        };
      };
    };
  };

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # Setup keyfile
  boot.initrd.secrets = {
    "/crypto_keyfile.bin" = null;
  };

  # Enable swap on luks
  boot.initrd.luks.devices."luks-0d131aa4-e19d-496b-845e-24ea1db6c7c9".device = "/dev/disk/by-uuid/0d131aa4-e19d-496b-845e-24ea1db6c7c9";
  boot.initrd.luks.devices."luks-0d131aa4-e19d-496b-845e-24ea1db6c7c9".keyFile = "/crypto_keyfile.bin";
}
