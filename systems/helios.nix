{ config, pkgs, ... }:

{
  imports = [
    ./helios-hardware.nix
    ./base.nix
  ];

  # Hostname
  networking.hostName = "Helios";

  # Packages
  environment.systemPackages = with pkgs; [
    go
    hugo
    wally-cli
    qmk
    gimp
    gzdoom
    superTuxKart
    virt-manager
    godot_4
    nvtopPackages.amd
    via
    retroarch
    krita
    blender-hip
  ];

  # Virt-Manager
  virtualisation.libvirtd.enable = true;
  virtualisation.spiceUSBRedirection.enable = true;

  # Hardware
  services.xserver.videoDrivers = [ "amdgpu" ];
  hardware.keyboard.qmk.enable = true;
  services.udev.packages = [ pkgs.via ];

  # Bootloader
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
}
